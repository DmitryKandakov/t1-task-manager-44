package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.endpoint.*;
import ru.t1.dkandakov.tm.api.service.*;
import ru.t1.dkandakov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.dkandakov.tm.endpoint.*;
import ru.t1.dkandakov.tm.service.*;
import ru.t1.dkandakov.tm.service.dto.ProjectDtoService;
import ru.t1.dkandakov.tm.service.dto.SessionDtoService;
import ru.t1.dkandakov.tm.service.dto.TaskDtoService;
import ru.t1.dkandakov.tm.service.dto.UserDtoService;
import ru.t1.dkandakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectServiceDTO projectService = new ProjectDtoService(connectionService);

    @Getter
    @NotNull
    private final ITaskServiceDTO taskService = new TaskDtoService(connectionService, projectService);

    @Getter
    @NotNull
    private final IUserServiceDTO userService = new UserDtoService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ISessionServiceDTO sessionService = new SessionDtoService(connectionService);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?WSDL",
                propertyService.getServerHost(),
                propertyService.getServerPort(),
                name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    public void start() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

}