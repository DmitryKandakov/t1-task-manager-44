package ru.t1.dkandakov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public interface CryptUtil {

    @NotNull
    String TYPE = "AES/ECB/PKCS5Padding";

    @NotNull
    String HASH_ALGORITHM = "SHA-1";

    @NotNull
    String CRYPTO_ALGORITHM = "AES";

    @NotNull
    @SneakyThrows
    static SecretKeySpec getKey(@NotNull final String secretKey) {
        @NotNull final MessageDigest sha = MessageDigest.getInstance(HASH_ALGORITHM);
        @NotNull final byte[] key = secretKey.getBytes(StandardCharsets.UTF_8);
        @NotNull final byte[] digest = sha.digest(key);
        @NotNull final byte[] secret = Arrays.copyOf(digest, 16);
        return new SecretKeySpec(secret, CRYPTO_ALGORITHM);
    }

    @NotNull
    @SneakyThrows
    static String encrypt(@NotNull String secret, @NotNull String strToEncrypt) {
        @NotNull final SecretKeySpec secretKeySpec = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        @NotNull final byte[] bytes = strToEncrypt.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @NotNull
    @SneakyThrows
    static String decrypt(@NotNull String secret, @NotNull String strToDecrypt) {
        @NotNull final SecretKeySpec secretKeySpec = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
    }

    static void main(String[] args) {
        System.out.println(encrypt("123", "HELLO"));
    }

}