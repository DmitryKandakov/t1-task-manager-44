package ru.t1.dkandakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @NotNull
    @Insert("INSERT INTO tm_user (id, login, password_hash, fst_name, lst_name, mdl_name, email, role, lock_flg) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{email}, #{role}, #{locked});")
    void add(@NotNull UserDTO user);

    @Nullable
    @Select("SELECT * from tm_user;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * from tm_user WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    UserDTO findOneById(@Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * from tm_user WHERE login = #{login} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    UserDTO findByLogin(@Param("login") @Nullable String login);

    @Nullable
    @Select("SELECT * from tm_user WHERE email = #{email} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    UserDTO findByEmail(@Param("email") @Nullable String email);

    @Delete("DELETE FROM tm_user WHERE id = #{id};")
    void remove(@NotNull UserDTO user);

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, fst_name = #{firstName}, lst_name = #{lastName}, " +
            "mdl_name = #{middleName}, email = #{email}, role = #{role}, lock_flg = #{locked} WHERE id = #{id};")
    void update(@NotNull UserDTO user);

}