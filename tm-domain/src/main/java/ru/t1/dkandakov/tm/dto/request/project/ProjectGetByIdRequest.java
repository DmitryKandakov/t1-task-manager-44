package ru.t1.dkandakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIdRequest extends AbstractIdRequest {

    public ProjectGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }

}