package ru.t1.dkandakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.model.IWBS;
import ru.t1.dkandakov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    public ProjectDTO(@NotNull final String name, @NotNull final Status status, @Nullable final String description) {
        this.name = name;
        this.status = status;
        this.description = description;
    }

    public ProjectDTO(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        super(userId);
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}