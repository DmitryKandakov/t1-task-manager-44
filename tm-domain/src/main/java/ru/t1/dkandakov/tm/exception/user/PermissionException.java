package ru.t1.dkandakov.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is not granted!");
    }

}
