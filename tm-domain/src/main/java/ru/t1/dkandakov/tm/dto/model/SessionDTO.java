package ru.t1.dkandakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column
    private Date date = new Date();

    @Nullable
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = null;

}