package ru.t1.dkandakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractIdRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(
            @Nullable String token,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token, id);
        this.name = name;
        this.description = description;
    }

}