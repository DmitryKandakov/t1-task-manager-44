package ru.t1.dkandakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public AbstractUserOwnedModelDTO(@Nullable String userId) {
        this.userId = userId;
    }

}
