package ru.t1.dkandakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "logout current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        getServiceLocator().getAuthEndpoint().logout(request);
        setToken(null);
    }


    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
