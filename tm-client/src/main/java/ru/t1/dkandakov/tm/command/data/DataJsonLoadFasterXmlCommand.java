package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataJsonLoadFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return "data-load-json";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}