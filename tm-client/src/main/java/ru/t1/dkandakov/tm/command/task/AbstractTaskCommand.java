package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.enumerated.Status;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task + ": " + task.getId());
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: + " + task.getId());
        System.out.println("NAME: + " + task.getName());
        System.out.println("DESCRIPTION: + " + task.getDescription());
        System.out.println("STATUS: + " + Status.toName(task.getStatus()));
    }

}