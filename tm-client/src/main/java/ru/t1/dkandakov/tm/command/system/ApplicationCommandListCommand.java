package ru.t1.dkandakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationCommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

}